# js-practice_flag-flix

Cute project to practice using the fetch API and responsive searching in Vanilla JS.

Built with HTML/CSS/JS.

Have a go: [search your country's flag here!!](https://condescending-perlman-89c9dd.netlify.com/)

