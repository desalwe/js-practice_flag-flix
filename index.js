let countryArray = [];
const fetchCountries = () => {
  fetch(`https://restcountries.eu/rest/v2/all`)
    .then(response => response.json())
    .then(data => {
      countryArray = data;
      renderAllCards();
    });
};


const renderAllCards = (countries = countryArray) => {
  const liGenerator = countries.map(country => {
    return cardHtml(country);
  }).join("");

  countryList.innerHTML = liGenerator;
};
fetchCountries();

const displayMatches = () => {
  const matches = filterFunc(countryArray, searchInput.value);
  const liGenerator = matches.map(country => {
    return cardHtml(country);
  }).join("");

  countryList.innerHTML = liGenerator;
};

const filterFunc = (countryArray, wordToMatch) => {
  const regex = new RegExp(wordToMatch, "gi");
  return countryArray.filter(country => {
    return country.name.match(regex) || country.region.match(regex) || country.subregion.match(regex);
  });
};

const cardHtml = (place) => {
  return `<li>
    <div class="card card-us" style="width: 18rem;">
      <div class="card-img-top" style="background-image: url('${place.flag}'); height: 140px; background-size: 100% 100%;   background-repeat: no-repeat; background-position: center;"></div >
      <div class="card-body">
        <h5 class="card-title">${place.name}</h5>
        <p class="card-text">${place.subregion ? place.subregion : ""}${place.subregion && place.region ? ",&nbsp&nbsp" : ""}${place.region ? place.region : ""}</p >
      </div >
    </div >
  </li >`;
};


const countryList = document.querySelector(".country-list");
const searchInput = document.getElementById("search-input");

searchInput.addEventListener("change", displayMatches);
searchInput.addEventListener("keyup", displayMatches);

window.onload = function () {
  document.getElementById("search-input").value = '';
};
